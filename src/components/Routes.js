import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from '../pages/Home'
import Homepage from '../pages/Homepage'
import FilmInfo from '../pages/FilmInfo'

class Routes extends React.Component {
    render() {
      return (
        <Switch>
          <Route path='/' exact component={Homepage} />
          <Route path='/film_info/:id' exact component={FilmInfo} />
        </Switch>
      );
    }
  }
  
  export default Routes;
  