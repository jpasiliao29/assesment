export const FILM_LIST = 'FILM_LIST';
export const FILM_LIST_SUCCESS = 'FILM_LIST_SUCCESS';
export const FILM_LIST_FAIL = 'FILM_LIST_FAIL';

export const SPECIFIC_FILM = 'SPECIFIC_FILM';
export const SPECIFIC_FILM_SUCCESS = 'SPECIFIC_FILM_SUCCESS';
export const SPECIFIC_FILM_FAIL = 'SPECIFIC_FILM_FAIL';

export const VIEW_PERSON = 'VIEW_PERSON';
export const VIEW_PERSON_SUCCESS = 'VIEW_PERSON_SUCCESS';
export const VIEW_PERSON_FAIL = 'VIEW_PERSON_FAIL';

export default function reducer(state = {
  filmListState: {},
  specificFilmState: {},
  viewPersonstate: {},
}, action) {
  switch (action.type) {

    case FILM_LIST:
      return { ...state, loading: true };
    case FILM_LIST_SUCCESS:
      return { ...state, loading: false, filmListState: action.payload.data };
    case FILM_LIST_FAIL:
      return { ...state, loading: false, filmListState: action.error.response.data };

    case SPECIFIC_FILM:
      return { ...state, loading: true };
    case SPECIFIC_FILM_SUCCESS:
      return { ...state, loading: false, specificFilmState: action.payload.data };
    case SPECIFIC_FILM_FAIL:
      return { ...state, loading: false, specificFilmState: action.error.response.data };


    case VIEW_PERSON:
      return { ...state, loading: true };
    case VIEW_PERSON_SUCCESS:
      return { ...state, loading: false, viewPersonstate: action.payload.data };
    case VIEW_PERSON_FAIL:
      return { ...state, loading: false, viewPersonstate: action.error.response.data };

    default:
      return state;
  }
}

export function filmList() {
  return {
    type: FILM_LIST,
    payload: {
      request: {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
        url: `/films`,
      }
    }
  };
}

export function specificFilm(id) {
  return {
    type: FILM_LIST,
    payload: {
      request: {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
        url: `/films/${id}`,
      }
    }
  };
}

export function specificPerson(url) {
  return {
    type: VIEW_PERSON,
    payload: {
      request: {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
        url,
      }
    }
  };
}