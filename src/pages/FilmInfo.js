
import { connect } from 'react-redux';
import { specificFilm, specificPerson } from '../reducer/reducer';
import React, { Component } from 'react'
import { MDBContainer, MDBCol, MDBTooltip, MDBBtn, MDBRow } from 'mdbreact'
import '../assets/styling/index.css'


class FilmInfo extends Component {
  state = {
    person: [],
    filmData: [],
  };

  componentWillMount(){
    this.Handle_Specific_Film()
  }

  Handle_Specific_Film = async () => {
    await this.props.specificFilm(this.props.match.params.id)
    // console.log(this.props.specificFilmState, 'specific film state')
    this.setState({filmData: this.props.specificFilmState}, () => this.Handle_Specific_Person())
  }

  Handle_Specific_Person = async () => {
    let people = []
    this.state.filmData.characters.map(async(character) => {
      await this.props.specificPerson(character)
      people.push(this.props.viewPersonstate)
      this.setState({person: people})
    })
  }

  Handle_Char=()=>{
    let data = this.state.person
    return(
      data.map((value)=>{ 
        return(
          <MDBCol md='6' lg='6' xl='6' className='text-left'>
            <MDBTooltip placement="right">
              <MDBBtn id='char' className='background-transparent border-none color-black ok-btn text-left mb-0 ml-3'>{value.name}</MDBBtn>
              <div>
                <p className='mb-0 text-left'>Name: {value.name}</p>
                <p className='mb-0 text-left'>Height: {value.height}</p>
                <p className='mb-0 text-left'>Mass: {value.mass}</p>
                <p className='mb-0 text-left'>Hair color: {value.hair_color}</p>
                <p className='mb-0 text-left'>eye color: {value.eye_color}</p>
                <p className='mb-0 text-left'>Birth year: {value.birth_year}</p>
                <p className='mb-0 text-left'>Gender: {value.gender}</p>                
              </div>
            </MDBTooltip>
          </MDBCol>
        )
      })
    )
  }

  render() {
    let {filmData} = this.state
    return (
      <MDBContainer className='mt-5 text-center border-gray mb-3'>
        <MDBCol lg='8' xl='8' className='inlined font-size-20 fantasy mb-3'>
          <MDBCol>
            <p className='font-size-30 py-3 font-weight-bold'>Title: {filmData.title}</p>
          </MDBCol>
          <MDBCol>
            <p className='text-left'>Directed by: {filmData.director}</p>
          </MDBCol>
          <MDBCol>
            <p className='text-left'>Producer: {filmData.producer}</p>
          </MDBCol>
          <MDBCol >
            <p className='text-left'>Created: {filmData.created}</p>
          </MDBCol>
          <MDBCol>
            <p className='text-left'>Release date: {filmData.release_date}</p>
          </MDBCol>
          <MDBCol>
            <p className='text-left'>Episode: {filmData.episode_id}</p>
          </MDBCol>
          <MDBCol>
            <p className='text-left'>Introduction: </p>
            <p className='text-left ml-3'>{filmData.opening_crawl}</p>
          </MDBCol>
          <MDBCol>
            <p className='text-left mb-0'>Characters: </p>
          </MDBCol>
          <MDBRow>
            {this.Handle_Char()}
          </MDBRow>
        </MDBCol>
      </MDBContainer>
    )
  }
}

const mapStateToProps = state =>{
  return {
    specificFilmState: state.filmListState,
    viewPersonstate: state.viewPersonstate
  };
};

const mapDispatchToProps = {
  specificFilm,
  specificPerson
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilmInfo);