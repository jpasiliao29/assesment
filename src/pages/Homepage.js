import React, { Component } from 'react'
import { MDBContainer, MDBCol, MDBRow, MDBIcon, MDBModal, MDBModalHeader, MDBModalBody, MDBBtn } from 'mdbreact'
import {Link} from 'react-router-dom'
import '../assets/styling/index.css'
import { connect } from 'react-redux';
import { filmList, specificFilm } from '../reducer/reducer';

class Homepage extends Component {
  state = {
    favorite: false,
    films : [],
    displayFilm: [],
    popup: false,
    filmTitle: '',
    text: ''
  };

  componentWillMount(){
    this.handleGet_List_Films()
  }
  

  handleGet_List_Films = async () =>{
    let {filmList} = this.props
    await filmList();
    this.setState({films: this.props.filmListState.results})
    let favorite = localStorage.getItem('favoriteorites')
    favorite = JSON.parse(favorite)
    this.render_Films_List(favorite, this.props.filmListState.results)
  }

  handle_Change = event => {
    let { films } = this.state;
    let keyword = event.target.value;
    let searchItems = [];
    if(keyword.length > 0){
      films.map(item => {
        if(item.title.toLowerCase().includes(keyword.toLowerCase())){
          searchItems.push(item);
        }
      })
    }else{
      searchItems = films;
    }

    let favorite = localStorage.getItem('favorites')
    favorite = JSON.parse(favorite)
    this.render_Films_List(favorite, searchItems)
  };


  handle_Add_favorite = (id,title) =>{
    let favorite = localStorage.getItem('favorites')
    favorite = JSON.parse(favorite)
    if(favorite == null){
      localStorage.setItem('favorites', JSON.stringify({ [id]:  0 }) )
    }else{
      if(favorite[id == undefined]) {
        favorite = {...favorite, [id]: 0 }
        localStorage.setItem('favorites', JSON.stringify(favorite))
      }else{
        favorite[id] = favorite[id] == 0 ? 1 : 0;
        localStorage.setItem('favorites', JSON.stringify(favorite))
        if(favorite[id] == 0){
          this.setState({
            popup: !this.state.popup,
            filmTitle: title,
            text: 'was added to favorites.'
          })
        }else{
          this.setState({
            popup: !this.state.popup,
            filmTitle: title,
            text: 'was remove from favorites.'
          })
      }
      }
    }
    let {films} = this.state
    this.render_Films_List(favorite, films)
  }

  render_Films_List = (favorite, data) => {
    let temp = [];
    if(favorite){
      data.map((val) => {
        if(favorite[val.episode_id] == undefined){
          val = {...val, favorite: 1 }
        }else{
          val = {...val, favorite: favorite[val.episode_id] }
        }
        temp.push(val)
      })
      temp.sort((yes,not) => {
        return yes.favorite - not.favorite;
      })
    }else{
      temp = data
    }

    let displayFilm = temp.map((value, index)=>{
        let isFav = value.favorite == 0 ? 'color-red' : '';
        return(
          <MDBRow className='pt-3 pb-2 mb-3 border-bottom font-size-20' key={index}>
            <MDBCol lg='1' xl='1'>
              <MDBIcon icon="heart" className={`${isFav} my-auto`} onClick={()=>{this.handle_Add_favorite(value.episode_id,value.title)}}/>
            </MDBCol>
            <MDBCol>
              <Link to={`/film_info/${value.url.split('/')[5]}`}>
                <p className='mb-0 text-left color-black'>{value.title}</p>
              </Link>
            </MDBCol>
            <MDBCol>
              <p className='mb-0 text-left'>Directed by: {value.director}</p>
            </MDBCol>
          </MDBRow>
        )
      })
    this.setState({ displayFilm });
  }

  toggle = nr => () => {
    let modalNumber = 'modal' + nr
    this.setState({
      [modalNumber]: !this.state[modalNumber]
    });
  }
  Modal() {
    return (
      <MDBContainer>
        <MDBModal isOpen={this.state.popup} toggle={this.toggle(1)}>
          <MDBModalBody className='px-3 pt-4'>
            <p className='font-size-30 mb-0 '>{this.state.filmTitle}</p>
            <p className='font-size-20 '>{this.state.text}</p>
            <MDBCol className='inlined'>
              <MDBBtn className='text-center ok-btn color-black background-color-gray' onClick={()=>{this.setState({popup: !this.state.popup})}}>OK</MDBBtn>
            </MDBCol>
          </MDBModalBody>
        </MDBModal>
      </MDBContainer>
      );
    }

  render() {
    let { films, displayFilm } = this.state
    return (
      <MDBContainer className='mt-5 text-center border-gray '>
        <MDBCol lg='8' xl='8' className='inlined mb-3'>
          <MDBCol className='float-center'>
            <MDBRow className='justify-content-between'>
              <MDBCol lg='4' xl='4' className='text-left px-0 mt-2'>
                <p className='font-size-40 mb-0'>Films</p>
              </MDBCol>
              <MDBCol lg='7' xl='7' className='my-auto px-0'>
                <input 
                  className='height-35 border-gray width-100 px-2'
                  placeholder='Search film title...'
                  onChange={this.handle_Change.bind(this)}
                />
              </MDBCol>
            </MDBRow>
          </MDBCol>
          <MDBCol>
          {films && displayFilm}
          </MDBCol>
        </MDBCol>
        {this.Modal()}
      </MDBContainer>
    )
  }
}

const mapStateToProps = state =>{
  return {
    filmListState: state.filmListState,
    specificFilmState: state.specificFilmState
  };
};

const mapDispatchToProps = {
  filmList,
  specificFilm
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Homepage);